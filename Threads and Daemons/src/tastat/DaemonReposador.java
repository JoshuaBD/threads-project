package tastat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

public class DaemonReposador extends Thread {
	Magatzem mgz;
	DiariMoviments dm;
	List<OrdreCompra> ordres = new ArrayList<OrdreCompra>();

	DaemonReposador(Magatzem mgz) {
		this.mgz = mgz;
	}

	public void run() {
		for (;;) {
			try {
				mgz.mirarCompres.acquire();
				List<OrdreCompra> a = new ArrayList<OrdreCompra>(mgz.getCompres());
				mgz.mirarCompres.release();
				
				if(a.size() != 0) {
					ListIterator<OrdreCompra> li = a.listIterator();
					//System.out.println(a.get(0));
					while (li.hasNext()) {
						OrdreCompra oc = (OrdreCompra) li.next();
	
						if (oc != null) {
							if (oc.estat != OrdreEstat.LLIURADA) {
								dm = new DiariMoviments();
								oc.getProducte().setStock(oc.getQuantitat());
								//oc.getOp().notify();
								//System.out.println("Posant m�s stock de " + oc.getProducte().getNomProducte() + " i posant "
										//+ oc.getQuantitat() + " | " + oc.getCom().getIdComanda());
	
								dm.setProducte(oc.getProducte());
								dm.setLot(0);
								dm.setTipusMoviment('S');
								dm.setOrigenMoviment("dr");
								dm.setQuantitat(oc.getProducte().getQuantitatCompra());
								dm.setObservacions("TEST");
								dm.setMoment(new Date());
								dm.setComanda(oc.getCom());
								
								mgz.mirarDiari.acquire();
								//System.out.println(dm.toString());
								mgz.getMoviments().add(dm);
								mgz.mirarDiari.release();
								
								oc.setEstat(OrdreEstat.LLIURADA);
							}
						}
					}
				}
				Thread.sleep(1000);

			} catch (InterruptedException e) {
				//e.printStackTrace();
			}

		}
	}
}
