package tastat;

import java.util.Date;

public class DiariMoviments {
	protected int numMoviment;
	protected Date moment;
	protected Producte producte;
	protected int lot;
	protected char tipusMoviment;
	protected int quantitat;
	protected String observacions;
	protected Comanda comanda;
	protected OrdreCompra ordreCompra;
	//saber de donde viene el moviment:
	protected String origenMoviment;
	
	DiariMoviments(){
		numMoviment = Generador.getNextDiariMoviment();
		moment = new Date();
	}
	
	public int getNumMoviment() {
		return numMoviment;
	}

	public void setNumMoviment(int numMoviment) {
		this.numMoviment = numMoviment;
	}

	public Date getMoment() {
		return moment;
	}

	public void setMoment(Date moment) {
		this.moment = moment;
	}

	public Producte getProducte() {
		return producte;
	}

	public void setProducte(Producte producte) {
		this.producte = producte;
	}

	public int getLot() {
		return lot;
	}

	public void setLot(int lot) {
		this.lot = lot;
	}

	public char getTipusMoviment() {
		return tipusMoviment;
	}

	public void setTipusMoviment(char tipusMoviment) {
		this.tipusMoviment = tipusMoviment;
	}

	public int getQuantitat() {
		return quantitat;
	}

	public void setQuantitat(int quantitat) {
		this.quantitat = quantitat;
	}

	public String getObservacions() {
		return observacions;
	}

	public void setObservacions(String observacions) {
		this.observacions = observacions;
	}

	public Comanda getComanda() {
		return comanda;
	}

	public void setComanda(Comanda comanda) {
		this.comanda = comanda;
	}

	public OrdreCompra getOrdreCompra() {
		return ordreCompra;
	}

	public void setOrdreCompra(OrdreCompra ordreCompra) {
		this.ordreCompra = ordreCompra;
	}
	
	//D'ON PROVE:
	public String getOrigenMoviment() {
		return origenMoviment;
	}

	public void setOrigenMoviment(String origenMoviment) {
		this.origenMoviment = origenMoviment;
	}
	
	DiariMoviments(Producte p, int l, char tipusM, int q, String obs) {
		this();
		producte = p;
		lot = l;
		tipusMoviment = tipusM;
		quantitat = q;
		observacions = obs;
	}
	//PASTEL ACABAT:
	DiariMoviments(Comanda cm ,Producte p, char tipusM, String obs,String om) {
		this();
		comanda=cm;
		producte = p;
		tipusMoviment = tipusM;
		observacions = obs;
		origenMoviment=om;
	}
	DiariMoviments(Comanda cm ,char tipusM, String obs, String om) {
		this();
		comanda=cm;
		tipusMoviment = tipusM;
		observacions = obs;
		origenMoviment=om;
	}
	
	@Override
	public String toString() {
		return "Moviment: REPOSAR PROD [numMoviment: " + numMoviment + ", moment: " + moment + ", producte: " + producte.getNomProducte() + ", lot: "
				+ lot + ", tipusMoviment: " + tipusMoviment + ", quantitat " + quantitat + ", observacions "
				+ ", comanda " + comanda.getIdComanda() + "]";
	}

	public String toStringTransport() {
		return "Moviment: COMANDA EN TRASPORT [numMoviment: "+numMoviment+" , moment: "+ moment+ ", tipusMoviment: " + tipusMoviment + ", observacions "
				+ observacions + ", comanda " + comanda.getIdComanda();
	}
	public String toStringPastelAcabat() {
		return "Moviment: PASTEL PREPARAT [numMoviment: "+numMoviment+" , moment: "+ moment+" , Producte: "+producte.getNomProducte()+ ", tipusMoviment: " + tipusMoviment + ", observacions "+observacions +
				", comanda " + comanda.getIdComanda();
	}
	public String toStringComandaAcabada() {
		return "Moviment: COMANDA PREPARADA [numMoviment: "+numMoviment+" , moment: "+ moment+ " , tipusMoviment: " + tipusMoviment + ", observacions "+observacions +
				", comanda " + comanda.getIdComanda();
	}
	
}