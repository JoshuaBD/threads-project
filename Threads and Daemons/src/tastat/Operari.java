package tastat;

import java.sql.PreparedStatement;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Operari extends Thread {

	protected int idOperari;
	protected String nomOperari;
	protected int numPastissos;
	protected Magatzem mgz;
	protected static Semaphore mirarComandes = new Semaphore(1);
	protected static int pastisos = 0;
	protected boolean dormir = false;
	protected long end;
	protected long start;
	protected DiariMoviments dm;
	protected boolean soltado = false;
	protected boolean broke = false;
	// protected Semaphore kill = new Semaphore(1);
	protected volatile boolean estat = true;
	protected static Semaphore pastisSimultani = new Semaphore(12);

	Operari() {
		idOperari = Generador.getNextOperari();
		Thread.currentThread().setName(Integer.toString(idOperari));
		numPastissos = 0;

	}

	Operari(Magatzem mgz) {
		this();
		this.mgz = mgz;
	}

	public int getNumPastissos() {
		return numPastissos;
	}

	public void setNumPastissos(int numPastissos) {
		this.numPastissos = numPastissos;
	}

	Operari(String nom) {
		this();
		nomOperari = nom;
	}

	@Override
	public String toString() {
		return (idOperari + ": " + getNomOperari());
	}

	public int getIdOperari() {
		return idOperari;
	}

	public String getNomOperari() {
		return nomOperari;
	}

	public void setNomOperari(String nomOperari) {
		this.nomOperari = nomOperari;
	}

	public void run() {
		Producte p;
		while (estat) {

			try {
				if (!dormir) {
					start = System.currentTimeMillis();
					end = start + 60000;
					dormir = true;
				}
				if (System.currentTimeMillis() >= end) {
					System.out.println("Operari " + this.getIdOperari() + " fa un descans de 5 minuts...");
					sleep(5000);
					System.out.println("Operari " + this.getIdOperari() + " torna a treballar...");
					dormir = false;
				}

				soltado = false;
				broke = false;
				mirarComandes.acquire();
				for (Comanda cm : mgz.getComandes()) {
					if (cm.getEstat() == ComandaEstat.PENDENT) {
						p = cm.rebaixarUnitat();

						//System.out.println("PERMISSIONS SIMULTANIS: " + pastisSimultani.availablePermits());
						if (p != null) {
							if (!soltado) {
								soltado = true;
								mirarComandes.release();
							}

							pastisSimultani.acquire();
							System.out.println("Operario " + this.getIdOperari() + " coge de la comanda "
									+ cm.getIdComanda() + " " + p.getNomProducte());

							ferProducte(p, cm); // FER PRODUCTE

							Thread.sleep((int) ((Math.random() * 1) + 1) * 1000);
							cm.unitatOK(p);
							System.out.println("Operario " + this.getIdOperari() + " ha fet el pastis de la comanda "
									+ cm.getIdComanda() + " " + p.getNomProducte());
							pastisSimultani.release();

							try {
								dm = new DiariMoviments(cm, p, 'S', "Pastis Fet", "op");
								dm.setMoment(new Date());

								mgz.mirarDiari.acquire();
								mgz.getMoviments().add(dm);
								mgz.mirarDiari.release();

							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}
						if (cm.isFinished() && cm.getEstat() == ComandaEstat.PENDENT) {
							if (cm.acabarComanda.tryAcquire(0, TimeUnit.MILLISECONDS)) {
								acabarComanda(cm);
								if (!soltado) {
									soltado = true;
									mirarComandes.release();
								}
								
								broke = true;
								break;
							}
						}
						

					}
					if (!soltado && broke) {
						soltado = true;
						mirarComandes.release();
					}

					if (!estat) {
						System.out.println("Operari " + this.getIdOperari() + " ha acabat i se l'ha emportat en Miquel...");
						//System.out.println(Thread.activeCount());
						if(Thread.activeCount() <= 8) {
							System.out.println("LA FABRICA S'HA QUEDAT SENSE OPERARIS!");
						}
						break;
					}
					// System.out.println(mirarComandes.availablePermits());

				}

			} catch (Exception e) {
			}
		}
	}

	public synchronized void acabarComanda(Comanda cm) {
		System.out.println("COMANDA " + cm.getIdComanda() + " PREPARADA!");
		cm.setEstat(ComandaEstat.PREPARADA);
		dm = new DiariMoviments(cm, 'S', "COMANDA PREPARADA", "cp");
		dm.setMoment(new Date());
		try {
			mgz.mirarDiari.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// System.out.println(dm.toString());
		mgz.getMoviments().add(dm);
		mgz.mirarDiari.release();
		cm.acabarComanda.release();
		mgz.getComandesPreparades().add(cm);
		mgz.getComandes().remove(cm);
	}

	public boolean ferProducte(Producte p, Comanda comanda) {
		int contador = 0;
		for (ProducteFabricacio pf : p.getFabricacio()) {
			contador++;
			// System.out.println("Soc Operari" + this.getIdOperari() + " i vaig a fer el
			// pas " + contador + " producte " + pf.getProducte().getNomProducte() + "(" +
			// pf.getProducte().getStock() + ")");
			try {
				pf.getProducte().restarStock.acquire();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			// System.out.println("Soc Operari" + this.getIdOperari() + " i TENGO EL PODER!
			// ");

			int stock;
			boolean ordreFeta = false;

			while (true) {
				stock = pf.getProducte().getStock();
				// System.out.println("Soc Operari" + this.getIdOperari() + ", reviso stock
				// producte " + pf.getProducte().getNomProducte() + "(" +
				// pf.getProducte().getStock() + ")");
				if (stock >= pf.getQuantitat()) {
					try {
						pf.getProducte().restarStock(pf.getQuantitat(), stock);
						pf.getProducte().restarStock.release();

						Thread.sleep(((int) (Math.random() * pf.getTempsMax()) + pf.getTempsMin()) * 1000);

						// System.out.println("Soc Operari" + this.getIdOperari() + " i NOOOOOOOOOO
						// TENGO EL PODER! ");
						break;
					} catch (Exception e) {

					}
				} else if (!ordreFeta) {
					// System.out.println("S'ha de fer ordre");
					OrdreCompra oc = new OrdreCompra();
					oc.setProducte(pf.getProducte());
					oc.setQuantitat(pf.getProducte().getQuantitatCompra() * 2);
					oc.setCom(comanda);
					try {
						mgz.mirarCompres.acquire();
						mgz.getCompres().add(oc);
						mgz.mirarCompres.release();

					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					pf.getProducte().setStock(pf.getQuantitat());

					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					ordreFeta = true;
				}
			}

		}
		return true;
	}

}