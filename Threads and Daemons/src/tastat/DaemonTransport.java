package tastat;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class DaemonTransport extends Thread{
	
	private Magatzem mgz;
	private int temps;
	private DiariMoviments dm;
	public DaemonTransport(Magatzem mgz, int temps) {
		this.mgz = mgz;
		this.temps = temps;
	}

	@Override
	public void run() {
		try {
			while (true) {
				if (mgz.getComandesPreparades().size() >= 1)// SOLO PUEDEN HABER 3
				{
					for(Comanda cm: mgz.getComandesPreparades()) {
						Transportar(cm);
						
						break;
					}				
				}
				Thread.sleep(temps);
			}

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void Transportar(Comanda cm) {
		try {
			dm=new DiariMoviments(cm,'S',"En trasport","dt");
			dm.setMoment(new Date());
			mgz.mirarDiari.acquire();
			//System.out.println(dm.toString());
			mgz.getMoviments().add(dm);
			mgz.mirarDiari.release();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("บบบบบบบบบComanda " + cm.getIdComanda() + " EN TRANSPORTบบบบบบบบบบบบ");
		
		cm.estat = ComandaEstat.TRANSPORT;
		mgz.getComandesTransportades().add(cm);//lo meto en mi lista
		mgz.getComandesPreparades().remove(cm);
		//cm.TransportarComanda.release();
	}
	
}
