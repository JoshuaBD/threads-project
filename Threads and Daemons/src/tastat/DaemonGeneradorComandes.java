package tastat;

import java.util.ArrayDeque;
import java.util.List;

public class DaemonGeneradorComandes extends Thread {
	int ordre = 0;
	private Magatzem mgz;
	Comanda c = null;
	protected List<ComandaLinia> linies;

	DaemonGeneradorComandes(Magatzem mgz) {
		this.mgz = mgz;
	}

	@Override
	public void run() {
		try {

			Producte pliv = null, pllim = null, peri = null, pvel = null;
			for (Producte p : mgz.getProductes()) {
				if (p.getNomProducte().equals("pLiviano"))
					pliv = p;
				if (p.getNomProducte().equals("pLlimona"))
					pllim = p;
				if (p.getNomProducte().equals("pErizo"))
					peri = p;
				if (p.getNomProducte().equals("pVelvet"))
					pvel = p;
			}

			while (true) {
				if (mgz.getComandes().size() <= 5) {
					int nClient = (int) (Math.random() * mgz.getClients().size());
					Client cli = mgz.getClients().get(nClient);
					Comanda m1 = new Comanda(cli);

					int tipus[] = new int[4];
					int contador = 0;
					for (int i = 0; i < 4; i++) {
						tipus[i] = (int) (Math.random() * 2); // If 0 no se hace, if 1 se hace

						if (tipus[i] == 1)
							contador++;
					}

					if (contador == 0)
						tipus[0] = 1;

					if (tipus[0] == 1)
						m1.getLinies().add(new ComandaLinia(pliv, (int) (Math.random() * 5) + 1));
					if (tipus[1] == 1)
						m1.getLinies().add(new ComandaLinia(pllim, (int) (Math.random() * 5) + 1));
					if (tipus[2] == 1)
						m1.getLinies().add(new ComandaLinia(peri, (int) (Math.random() * 5) + 1));
					if (tipus[3] == 1)
						m1.getLinies().add(new ComandaLinia(pvel, (int) (Math.random() * 5) + 1));

					mgz.getComandes().add(m1);
					Thread.sleep(10000);
				}
			}
		} catch (Exception e) {
		}
		;
	}
}
