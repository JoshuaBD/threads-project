package tastat;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class DaemonEscriptor extends Thread {
	Magatzem mgtz;

	DaemonEscriptor(Magatzem mgtz) {
		this.mgtz = mgtz;
	}

	public void run() {
		try {
			while (true) {
				Thread.sleep(10000);
				//System.out.println("-----------------> Entro DaemonEscriptor");
				mgtz.mirarDiari.acquire();
				List<DiariMoviments> a = new ArrayList<DiariMoviments>(mgtz.getMoviments());
				mgtz.mirarDiari.release();

				File f = new File("moviments.txt");
				FileWriter fw = new FileWriter(f);
				BufferedWriter bw = new BufferedWriter(fw);
				
				//System.out.println("------->" + a.get(0));
				for (DiariMoviments dm : a) {
					// System.out.println(dm.toString());
					if(dm.getOrigenMoviment().equalsIgnoreCase("dr")) {
						bw.write(dm.toString());
						bw.newLine();
					}else if(dm.getOrigenMoviment().equalsIgnoreCase("dt")){
						bw.write(dm.toStringTransport());
						bw.newLine();
					}else if(dm.getOrigenMoviment().equalsIgnoreCase("op")) {
						bw.write(dm.toStringPastelAcabat());
						bw.newLine();
					} else if(dm.getOrigenMoviment().equalsIgnoreCase("cp")) {
						bw.write(dm.toStringComandaAcabada());
						bw.newLine();
					}
					
					// System.out.println(mgtz.getMoviments().toString());
				}

				bw.flush();
				bw.close();
				fw.close();
				Thread.sleep(3000);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
