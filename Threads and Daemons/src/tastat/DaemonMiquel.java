package tastat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Semaphore;

public class DaemonMiquel extends Thread {
	protected List<Operari> elsMeusOperaris;
	protected ArrayList<Integer> numsop = new ArrayList<Integer>();
	protected Magatzem mgtz;

	DaemonMiquel(Magatzem elMeuMagatzem, List<Operari> elsMeusOperaris) {
		// TODO Auto-generated constructor stub
		this.elsMeusOperaris = elsMeusOperaris;
		mgtz = elMeuMagatzem;
	}

	public void run() {
		int count = 0;
		while (true) {
			try {

				//System.out.println("SOY MIQUEL! " + count);

				sleep(6000);
				if (count == 5) {
					count = 0;
					if (elsMeusOperaris.size() != 0) {
						Collections.shuffle(elsMeusOperaris);
						System.out.println("Miquel s'emportar� l'operari " + elsMeusOperaris.get(0).getIdOperari() + " quan acabi el que est� fent...");
						elsMeusOperaris.get(0).estat = false;
						elsMeusOperaris.remove(0);
						
						System.out.println("MIQUEL DESCANSAR� 10 SEGONS...");
						sleep(10000);
						
						// elsMeusOperaris.get(0).interrupt();
						// elsMeusOperaris.remove(0);
					}
				} else {
					count++;
				}

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
